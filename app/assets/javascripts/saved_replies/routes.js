import IndexComponent from './pages/index.vue';

export default [
  {
    path: '/',
    component: IndexComponent,
  },
];
